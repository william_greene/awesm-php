<?php

class Awesm
{

	private $apiKey;
	private $createHost = 'http://api.awe.sm';
	private $statsHost = 'http://api.awe.sm';
	private $apiVersion = 2;
	private $createApiVersion = 3;

	// endpoints (JSON only)
	const API_CREATE							= '/url.json';
	const API_CREATE_STATIC				= '/url/static.json';
	const API_CREATE_RETWEET			= '/url/retweet.json';
	const API_STATS_SHARES				= '/shares.json';
	const API_STATS_SHARES_LIST		= '/shares/list.json';
	const API_STATS_CLICKS				= '/clicks.json';
	const API_STATS_CLICK_DETAILS	= '/clicks/';
	const API_STATS_CLICKS_LIST		= '/clicks/list.json';
	const API_STATS_ACTIVITY			= '/activity.json';

	// dimension names
	const DIM_ORIGINAL_URL = 'original_url';
	const DIM_DOMAIN = 'domain';
	const DIM_CAMPAIGN = 'campaign';
	const DIM_CHANNEL = 'channel';
	const DIM_TOOL = 'tool';
	const DIM_USER = 'user_id';
	const DIM_NOTES = 'notes';
	const DIM_PARENT = 'parent_awesm';
	const DIM_SHARER = 'sharer_id';
	const DIM_AWESM_URL = 'awesm_url';

	// dimensions (for validation)
	private static $DIMENSIONS = array(
		self::DIM_ORIGINAL_URL,
		self::DIM_DOMAIN,
		self::DIM_CAMPAIGN,
		self::DIM_CHANNEL,
		self::DIM_TOOL,
		self::DIM_USER,
		self::DIM_NOTES,
		self::DIM_PARENT,
		self::DIM_SHARER,
		self::DIM_AWESM_URL
	);

	// interval names
	const INTERVAL_HOUR = 'hour';
	const INTERVAL_DAY = 'day';
	const INTERVAL_WEEK = 'week';

	// intervals (for validation)
	private static $INTERVALS = array(
		self::INTERVAL_HOUR,
		self::INTERVAL_DAY,
		self::INTERVAL_WEEK
	);

	/**
	 * Get an Awe.sm object. You can override API endpoint defaults here.
	 * @param <type> $apiKey
	 * @param <type> $defaults
	 */
	public function __construct($apiKey, $defaults=null)
	{
		$this->apiKey = $apiKey;
		if (!is_null($defaults))
		{
			if (array_key_exists('create_host', $defaults))
			{
				$this->createHost = $defaults['create_host'];
			}
			if (array_key_exists('stats_host', $defaults))
			{
				$this->statsHost = $defaults['stats_host'];
			}
			if (array_key_exists('api_version', $defaults))
			{
				$this->apiVersion = $defaults['api_version'];
			}
			if (array_key_exists('create_api_version', $defaults))
			{
				$this->createApiVersion = $defaults['create_api_version'];
			}
		}
	}

	/**
	 * Create an awe.sm URL.
	 * @param Array $params Accepts a set of named parameters:
	 * url				required
	 * channel		required
	 * tool				required
	 * campaign
	 * notes
	 * user_id
	 * parent
	 * domain
	 * awesm_id
	 * See docs for details. api_key and version parameters are set at instantiation.
	 * Callback is unavailable as it makes no sense in this context.
	 * @return Awesm_Url
	 */
	public function create($params)
	{
		return $this->callCreate(self::API_CREATE, $params);
	}

	/**
	 * Create a placeholder awe.sm URL.
	 * This is a working redirect that has the parameters applied, but
	 * only one is created for each set of parameters. This allows you to create
	 * a placeholder link for example purposes, then create the "real" link
	 * when you get additional data later.
	 * @param <type> $params
	 * @return <type>
	 */
	public function createStatic($params)
	{
		return $this->callCreate(self::API_CREATE_STATIC, $params);
	}

	/**
	 * From a placeholder ("static") awe.sm URL, create a new, unique awe.sm URL.
	 * @param <type> $params
	 * @return <type>
	 */
	public function createRetweet($params)
	{
		return $this->callCreate(self::API_CREATE_RETWEET, $params);
	}
	
	/**
	 * cURL calls the API
	 * @param <create|create-static|stats> $method API method being invoked
	 * @param <array> $params Parameters to be sent to the API
	 * @param <native|json|txt> $format Format of return. Defaults to native PHP associative array from JSON.
	 * @param <int> $timeout Defaults to 5 seconds. May need to be increased for larger stats calls.
	 * @return <type>
	 */
	private function callCreate($base, $params, $timeout=5)
	{
		// insert our default params
		$params['v'] = $this->createApiVersion;
		$params['key'] = $this->apiKey;

		$url = $this->createHost . $base;
		error_log("Awesm::callCreate: $url\n".print_r($params,true));

		// Create and execute cURL request
		$request = curl_init();
		curl_setopt($request, CURLOPT_URL, $url);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($request, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($request, CURLOPT_POST, 1);
		curl_setopt($request, CURLOPT_POSTFIELDS, $params);
		$response = curl_exec($request);
		$responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);	// HTTP code for determining success
		curl_close($request);
		
		if ($responseCode == 200) {
			// Success: response should be valid JSON
			// Errors come in plain text, so this is one last check.
			$result = json_decode($response, true);
			
			if ($result) {
				error_log("Create API success: " . $response);
				return $result;
			} else {
				error_log("Create API JSON error: (" . $responseCode . ") " . $response);
				return array( 'error' => $response );
			}
		} else {	// Create API error
			error_log("Create API error: (" . $responseCode . ") " . $response);
			return array( 'error' => $response );	
		}
	}

	/**
	 * Call a path on the stats host
	 * @param string $base API path; must have leading slash.
	 * @param array $param Dimensions and filters
	 * @param boolean $bApiKey Whether to include the API key; nearly always true
	 */
	private function fetchStats($base, $params, $bApiKey=true)
	{
		// insert implicit params
		$params['version'] = $this->apiVersion;
		if ($bApiKey) $params['api_key'] = $this->apiKey;

		// turn params into request parameters
		$kvPairs = array();
		foreach ($params as $param => $value)
		{
			if (!empty($value)) $kvPairs[] = urlencode($param) . '=' . urlencode($value);
		}

		// combine arguments with endpoint
		$path = '?' . implode('&', $kvPairs);
		$url = $this->statsHost . $base . $path;
		error_log("Fetching stats: $url");

		// call server
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		error_log("Full response: " . $response);

		// parse response
		$result = json_decode($response, true);
		if (empty($result))
		{
			$result = $response;
		}
		return $result;
	}

	/**
	 * Shares lookup endpoint.
	 * @param <type> $after
	 * @param <type> $before
	 * @param <type> $dimensions
	 * @param string $interval
	 * @return <type>
	 */
	public function getSharesLookup(
					$after=null,
					$before=null,
					$dimensions=null,
					$interval=null,
					$groupBy=null
	)
	{
		$params = array();

		$params = $this->validateDate('created_before',$before,$params);
		$params = $this->validateDate('created_after',$after,$params);
		$params = $this->validateDimensions($dimensions,$params);
		$params = $this->validateInterval($interval,$params);
		$params = $this->validateGroupBy($groupBy,$params);

		return $this->fetchStats(self::API_STATS_SHARES, $params);
	}

	/**
	 * Shares list endpoint.
	 * @param <type> $after
	 * @param <type> $before
	 * @param <type> $dimensions
	 * @return <type>
	 */
	public function getSharesList(
					$after=null,
					$before=null,
					$dimensions=null,
					$pageNumber=null,
					$perPage=null
	)
	{
		$params = array();

		$params = $this->validateDate('created_before',$before,$params);
		$params = $this->validateDate('created_after',$after,$params);
		$params = $this->validateDimensions($dimensions,$params);
		$params = $this->validatePageNumber($pageNumber,$params);
		$params = $this->validatePerPage($perPage,$params);

		return $this->fetchStats(self::API_STATS_SHARES_LIST, $params);
	}
	
	/**
	 * Click details endpoint
	 * @param <type> $domain
	 * @param <type> $stub
	 * @param <type> $after
	 * @param <type> $before
	 * @param <type> $interval
	 * @return <type> 
	 */
	public function getClickDetailsByDomainAndAwesmId(
					$domain, 
					$awesmId, 
					$after=null, 
					$before=null, 
					$interval=null,
					$withMetadata=false
	)
	{

		$awesmUrl = $this->validateAwesmUrl($domain,$awesmId);
		$params = $this->validateDate('clicked_before',$before,$params);
		$params = $this->validateDate('clicked_after',$after,$params);
		$params = $this->validateInterval($interval, $params);
		$params = $this->validateWithMetadata($withMetadata, $params);

		return $this->fetchStats(self::API_STATS_CLICK_DETAILS . $awesmUrl . '.json', $params);
	}
	
	public function getClickDetailsByAwesmUrl(
					$awesmUrl,
					$after=null, 
					$before=null, 
					$interval=null,
					$withMetadata=false
	)
	{

		// TODO: validate awesmUrl
		$params = $this->validateDate('clicked_before',$before,$params);
		$params = $this->validateDate('clicked_after',$after,$params);
		$params = $this->validateInterval($interval, $params);
		$params = $this->validateWithMetadata($withMetadata, $params);

		return $this->fetchStats(self::API_STATS_CLICK_DETAILS . $awesmUrl . '.json', $params);
	}

	/**
	 * Clicks lookup endpoint
	 * @param <type> $after
	 * @param <type> $before
	 * @param <type> $dimensions
	 * @param <type> $interval
	 * @param <type> $groupBy
	 * @return <type>
	 */
	public function getClicksLookup(
					$after=null,
					$before=null,
					$dimensions=null,
					$interval=null,
					$groupBy=null,
					$withMetadata=false
	)
	{
		$params = array();

		$params = $this->validateDate('clicked_before',$before,$params);
		$params = $this->validateDate('clicked_after',$after,$params);
		$params = $this->validateDimensions($dimensions,$params);
		$params = $this->validateInterval($interval,$params);
		$params = $this->validateGroupBy($groupBy,$params);
		$params = $this->validateWithMetadata($withMetadata, $params);

		return $this->fetchStats(self::API_STATS_CLICKS, $params);
	}

	/**
	 * Clicks list endpoint
	 * @param <type> $shareList
	 * @param <type> $after
	 * @param <type> $before
	 * @param <type> $interval
	 * @param <type> $totals
	 * @param <type> $pageNumber
	 * @param <type> $perPage
	 * @return <type>
	 */
	public function getClicksList(
					$shareList=null,
					$after=null,
					$before=null,
					$interval=null,
					$totals=false,
					$pageNumber=null,
					$perPage=null,
					$withMetadata=false
	)
	{
		$params = array();
		if (empty($shareList) || !is_array($shareList))
		{
			throw new Exception("Must provide a list of shares as an array");
		}

		$shareValues = implode(",", $shareList);
		$params['share_list'] = $shareValues;

		$params = $this->validateDate('clicked_before',$before,$params);
		$params = $this->validateDate('clicked_after',$after,$params);
		$params = $this->validateInterval($interval,$params);
		if (!empty($totals)) {
			if($totals === true)
			{
				$params['with_totals'] = 'true';
			}
			else
			{
				$params['with_totals'] = 'false';
			}
		}
		$params = $this->validatePageNumber($pageNumber,$params);
		$params = $this->validatePerPage($perPage,$params);
		$params = $this->validateWithMetadata($withMetadata, $params);

		return $this->fetchStats(self::API_STATS_CLICKS_LIST, $params);
	}


	////// validation functions

	private function validateDate($key,$value,$params)
	{
		// TODO: better date validation
		if (!empty($value)) {
			$params[$key] = $value;
		}
		return $params;
	}

	private function validateDimensions($dimensions,$params)
	{
		if (is_array($dimensions))
		{
			foreach ($dimensions as $dimension => $value)
			{
				if(!in_array($dimension,self::$DIMENSIONS))
				{
					throw new Exception("$dimension is not a valid filter dimension");
				}
				$params[$dimension] = $value;
			}
		}
		return $params;
	}

	private function validateInterval($interval,$params)
	{
		if (!empty($interval))
		{
			if (!in_array($interval,self::$INTERVALS))
			{
				throw new Exception("$interval is not a valid interval");
			}
			$params['interval'] = $interval;
		}
		return $params;
	}

	private function validateGroupBy($groupBy,$params)
	{
		if (!empty($groupBy))
		{
			if (!in_array($groupBy,self::$DIMENSIONS))
			{
				throw new Exception("$groupBy is not a valid group-by dimension");
			}
			$params['group_by'] = $groupBy;
		}
		return $params;
	}

	private function validatePageNumber($pageNumber,$params)
	{
		// TODO: better limiting
		if (!empty($pageNumber))
		{
			if (!is_numeric($pageNumber))
			{
				throw new Exception("Page number must be numeric; got $pageNumber");
			}
			$params['page'] = $pageNumber;
		}
		return $params;
	}

	private function validatePerPage($perPage,$params)
	{
		// TODO: better limiting
		if (!empty($perPage))
		{
			if (!is_numeric($perPage))
			{
				throw new Exception("Page size must be numeric; got $perPage");
			}
			$params['per_page'] = $perPage;
		}
		return $params;
	}
	
	private function validateWithMetadata($withMetadata,$params)
	{
		if (!empty($withMetadata))
		{
			if (!is_bool($withMetadata))
			{
				throw new Exception("with_metadata must be boolean; got $perPage");
			}
			$params['with_metadata'] = ($withMetadata === TRUE)? 'true' : 'false';
		}
		return $params;
	}

	private function validateAwesmUrl($domain,$stub)
	{
		// TODO: better checking
		if (empty($domain)) throw new Exception("awesm URL must have domain");
		if (empty($stub)) throw new Exception("awesm URL must have awesm ");
		return "{$domain}_{$stub}";
	}

//////////// TODO: not yet adapted to new format

	private function v1_getInfoPublic($stub, $domain=null, $format='json')
	{
		return $this->v1_getInfo($stub, $domain, $format, false);
	}

	private function v1_getInfo($stub, $domain=null, $format='json', $bApiKey=true)
	{
		$params = array();
		if (!empty($domain))
		{
			$params['domain'] = $domain;
		}
		$url = '/url/' . $stub . '.' . $format;

		return $this->fetchStats($url, $params, $format, $bApiKey);
	}

	private function v1_lookup(
	$start=null, $end=null, $createdStart=null, $createdEnd=null, $domain=null, $originalUrl=null, $shareType=null, $createType=null, $userId=null, $notes=null, $parentAwesm=null, $sharerId=null, $clicked=null, $details=false, $page=null, $perPage=null, $format='json', $bApiKey=true
	)
	{

		$url = '/url.' . $format;
		$params = array(
				'start_date' => $start,
				'end_date' => $end,
				'created_at_start' => $createdStart,
				'created_at_end' => $createdEnd,
				'domain' => $domain,
				'original_url' => $originalUrl,
				'share_type' => $shareType,
				'create_type' => $createType,
				'user_id' => $userId,
				'notes' => $notes,
				'parent_awesm' => $parentAwesm,
				'sharer_id' => $sharerId,
				'clicked' => $clicked,
				'details' => $details,
				'page' => $page,
				'per_page' => $perPage
		);

		return $this->fetchStats($url, $params, $format, $bApiKey);
	}




}

