<?php

/**
 * Tests for the awe.sm API library class.
 * These just check that the class can make calls and get results.
 * Tests of the API functionality is in their respective repos.
 */

require('../library/Awesm.php');

class AwesmTest extends PHPUnit_Framework_TestCase
{

	private $awesm;

	public function setUp()
	{
		$this->awesm = new Awesm('5c8b1a212434c2153c2f2c2f2c765a36140add243bf6eae876345f8fd11045d9');
	}

	public function testCreate()
	{
		$params = array(
			'url' => 'http://example.com/long/url/goes/here.aspx',
			'tool' => 'mKU7uN',
			'channel' => 'twitter',
			'domain' => 'demo.awe.sm'
		);
		$output = $this->awesm->create($params);

		$this->assertArrayHasKey('original_url',$output);
	}

	public function testStatic()
	{
		$params = array(
			'url' => 'http://example.com/long/url/goes/here.aspx',
			'tool' => 'mKU7uN',
			'channel' => 'twitter',
			'domain' => 'demo.awe.sm'
		);
		$output = $this->awesm->createStatic($params);

		$this->assertArrayHasKey('original_url',$output);

	}

	public function testError()
	{
		$params = array(
			'url' => null, // no url = problem
			'tool' => 'mKU7uN',
			'channel' => 'twitter',
			'domain' => 'demo.awe.sm'
		);
		$output = $this->awesm->create($params);

		$this->assertArrayHasKey('error',$output);
	}

	public function testSharesLookupAfterOnly()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01'
						);

		$this->assertEquals('348',$output['total_shares']);

	}

	public function testSharesLookupBeforeOnly()
	{
		$output = $this->awesm->getSharesLookup(
						null,
						'2010-10-01'
						);

		$this->assertEquals('22737',$output['total_shares']);

	}

	public function testSharesLookupBeforeAfterOnly()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01',
						'2010-10-30'
						);

		$this->assertEquals('151',$output['total_shares']);

	}

	public function testSharesLookupBeforeAfterGroupBy()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01',
						'2010-10-30',
						null,
						null,
						Awesm::DIM_CHANNEL
						);

		$this->assertEquals('151',$output['total_shares']);
		$this->assertEquals('channel',$output['group_by']);
		$this->assertEquals('9',$output['groups'][0]['shares']);
		$this->assertEquals('copypaste',$output['groups'][0]['channel']);

	}

	public function testSharesLookupBeforeAfterFilterGroupBy()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter'
						),
						null,
						Awesm::DIM_TOOL
						);

		$this->assertEquals('47',$output['total_shares']);
		$this->assertEquals('twitter',$output['channel']);
		$this->assertEquals('21',$output['groups'][4]['shares']);
		$this->assertEquals('api',$output['groups'][4]['tool']);

	}

	public function testSharesLookupBeforeAfterDoubleFilterGroupBy()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						),
						null,
						Awesm::DIM_ORIGINAL_URL
						);

		$this->assertEquals('21',$output['total_shares']);
		$this->assertEquals('twitter',$output['channel']);
		$this->assertEquals('api',$output['tool']);
		$this->assertEquals('13',$output['groups'][0]['shares']);
		$this->assertEquals('http://google.com/',$output['groups'][0]['original_url']);

	}

	public function testSharesLookupBeforeAfterDoubleFilterIntervalOnly()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						),
						Awesm::INTERVAL_HOUR
						);

		$this->assertEquals('21',$output['total_shares']);
		$this->assertEquals('twitter',$output['channel']);
		$this->assertEquals('api',$output['tool']);
		$this->assertEquals('1',$output['intervals'][0]['shares']);
		$this->assertEquals('2010-10-12 20:00:00',$output['intervals'][0]['hour']);

	}

	public function testSharesLookupBeforeAfterDoubleFilterIntervalGroupBy()
	{
		$output = $this->awesm->getSharesLookup(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						),
						Awesm::INTERVAL_HOUR,
						Awesm::DIM_ORIGINAL_URL
						);

		$this->assertEquals('21',$output['total_shares']);
		$this->assertEquals('twitter',$output['channel']);
		$this->assertEquals('api',$output['tool']);
		$this->assertEquals('1',$output['groups'][0]['intervals'][0]['shares']);
		$this->assertEquals('2010-10-12 20:00:00',$output['groups'][0]['intervals'][0]['hour']);
		
	}

	public function testSharesListBeforeAfterDoubleFilter()
	{
		$output = $this->awesm->getSharesList(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						)
						);

		print_r($output);

	}

	public function testSharesListBeforeAfterDoubleFilterPaged()
	{
		$output = $this->awesm->getSharesList(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						),
						2
						);

		print_r($output);

	}

	public function testSharesListBeforeAfterDoubleFilterPageSize()
	{
		$output = $this->awesm->getSharesList(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						),
						null,
						100
						);

		print_r($output);

	}

	public function testClickDetailsByDomainAndAwesmIdOnly()
	{
		$output = $this->awesm->getClickDetailsByDomainAndAwesmId(
						'demo.awe.sm',
						'20'
						);

		print_r($output);

	}

	public function testClickDetailsByAwesmUrlOnly()
	{
		$output = $this->awesm->getClickDetailsByAwesmUrl(
						'demo.awe.sm_20'
						);

		print_r($output);

	}

	public function testClicksLookup()
	{
		$output = $this->awesm->getClicksLookup(
						'2010-10-01',
						'2010-10-30',
						array(
								Awesm::DIM_CHANNEL => 'twitter',
								Awesm::DIM_TOOL => 'api'
						),
						Awesm::INTERVAL_HOUR,
						Awesm::DIM_AWESM_URL
						);

		print_r($output);

	}

	public function testClicksList()
	{
		$output = $this->awesm->getClicksList(
						array('demo.awe.sm_20','demo.awe.sm_21','demo.awe.sm_22')
						);

		print_r($output);

	}

}